# About
This project was an attempt to create a small gui around [aviglitch](https://github.com/ucnv/aviglitch) using Shoes.
Since Shoes is now an abandonware this project will not be continued. 

# Compile the project
## OSX
```
cd script/utils
cshoes -f packager_osx.rb
```
## Window
```
cd script/utils
cshoes --ruby exe_packager_win32.rb exe_info_win32.yml
```
# Create new release
```
cd script
./create_new_version.sh <version number>
```

