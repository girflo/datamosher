# run this with ruby osx-merge.rb file.yaml
require 'fileutils'
include FileUtils
require 'yaml'
opts = YAML.load_file(ARGV[0])
opts['shoes_at'] = '/Applications/Shoes.app/Contents/MacOS'
opts['target_ruby'] = '2.3.0'
opts['target_ruby_arch'] = 'x86_64-darwin-14' # yosemite 10.10
if opts['shoes_at']
  DIR = opts['shoes_at']
else
  DIR = "/Applications/Shoes.app/Contents/MacOS" # EXPECT A FAIL
end
opts['packdir'] = Dir.getwd
home = ENV['HOME']
GEMS_DIR = File.join(home, '.shoes','+gem')
puts "DIR = #{DIR}"
puts "GEMS_DIR = #{GEMS_DIR}"
require "#{DIR}/lib/package/merge-osx"
PackShoes::merge_osx(opts) {|t| $stderr.puts t}
