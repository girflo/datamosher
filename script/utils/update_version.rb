require 'optparse'
require 'yaml'

def update_version_in_file(file_path, version)
  data = YAML.load_file file_path
  data["app_version"] = version
  File.open(file_path, 'w') { |f| YAML.dump(data, f) }
end

OptionParser.new do |opts|
  opts.banner = 'Usage: update_version.rb -v version'

  opts.on("-v", "--version VERSION",
          "Require the VersION before executing your script") do |version|
    update_version_in_file(File.join(File.dirname(__FILE__), "./../../src", 'application.yml'), version)
    update_version_in_file(File.join(__dir__,"dmg_info_osx.yml"), version)
    update_version_in_file(File.join(__dir__,"exe_info_win32.yml"), version)
  end

  opts.on_tail('-h', '--help', 'Show this message') do
    puts opts
    exit
  end
end.parse!

