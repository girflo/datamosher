# coding: utf-8
Shoes.setup do
  gem 'aviglitch'
  gem 'concurrent-ruby'
end
require 'yaml'
require_relative 'views/screens/content/about.rb'
require_relative 'views/screens/content/help.rb'
require_relative 'views/screens/content/home.rb'
require_relative 'views/screens/content/queue.rb'
require_relative 'views/screens/base.rb'

class Ui < Shoes
  url '/',       :home_screen
  url '/about',  :about_screen
  url '/help',   :help_screen
  url '/queue',  :queue_screen

  def about_screen
    background Colors.app_background
    screen_base(screen_content_about)
  end

  def help_screen
    background Colors.app_background
    screen_base(screen_content_help)
  end

  def home_screen
    background Colors.app_background
    screen_base(screen_content_home)
  end

  def queue_screen
    background Colors.app_background
    screen_base(screen_content_queue)
  end
end

Shoes.app(app_name: 'Datamosher', title: 'Datamosher', width: 1400, height: 800, menus: true)
