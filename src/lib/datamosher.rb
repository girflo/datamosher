class Datamosher
  require 'aviglitch'

  def initialize(input_file, input_folder, output_file, output_folder)
    if input_file.nil?
      @input_path = input_folder
      @input_type = 'directory'
    else
      @input_path = input_file
      @input_type = 'image'
    end
    @output_file = output_file
    @glitch_folder = output_folder
  end

  def call
    if @input_type == 'directory'
      Dir.foreach(@input_path) do |item|
        next unless item.end_with?('.avi')

        input_filename = "#{@input_path}/#{item}"
        output_filename = "#{@glitch_folder}/#{@output_file}_#{item}.avi"
        create_glitch(input_filename, output_filename)
      end
    else
      output_filename = "#{@glitch_folder}/#{@output_file}.avi"
      create_glitch(@input_path, output_filename)
    end
  end

  private

  def create_glitch(input_path, output_filename)
    avi = AviGlitch.open(input_path)
    avi.glitch(:keyframe) do |data|
      data.gsub(/\d/, '0')
    end
    avi.output(output_filename)
  end
end
