class DatamosherAsync
  require 'concurrent'
  require_relative 'datamosher.rb'
  include Concurrent::Async

  def initialize(input_file, input_folder, output_file, output_folder)
    super()
    @glitcher = Datamosher.new(input_file, input_folder, output_file, output_folder)
    @input_type = input_file.nil? ? 'directory' : 'image'
  end

  def call
    Thread.current[:name] = "Datamoshing"
    @glitcher.call
    Thread.current.exit
  end
end
