require_relative '../../lib/datamosher_async.rb'
require_relative '../templates/banner.rb'
require_relative '../templates/colors'
require_relative '../templates/menus.rb'
require_relative '../templates/navigation.rb'
require_relative '../templates/section_title.rb'
require_relative '../templates/form/messages.rb'
require_relative '../templates/form/io.rb'
APPLICATION = YAML.load_file('application.yml')
CONTENT = YAML.load_file('static/content.yml')

class Shoes::ScreenBase < Shoes::Widget
  attr_reader :messages

  def initialize(content)
    @content = content
    background Colors.app_background
    menus
    banner
    @messages = template_form_messages(CONTENT['error_title'])
    stack margin_left: 40, margin_right: 40 do 
      screen_content
    end
  end

  private

  def screen_content
    @content.call(self)
  end
end
