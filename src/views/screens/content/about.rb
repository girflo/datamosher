class Shoes::ScreenContentAbout < Shoes::Widget
  def call(screen)
    @screen = screen
    navigation
    stack do
      background Colors.grey
      stack margin_bottom: 20, margin_top: 20 do
        subtitle CONTENT['about']['title'], align: 'center'
        stack margin_left: 20 do
          tagline "#{CONTENT['about']['version_text']} #{APPLICATION['app_version']}"
          tagline CONTENT['about']['subtitle']
          stack margin_left: 20 do
            para CONTENT['about']['content']
          end
        end
      end
    end
  end
end
