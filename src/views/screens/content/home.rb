class Shoes::ScreenContentHome < Shoes::Widget
  def call(screen)
    @screen = screen
    navigation('datamosher')
    @glitch_io = template_form_io('.avi')
    send_button
  end

  private

  def send_button
    flow margin_top: 12 do
      stack width: 140, height: 40 do
        background Colors.green
        @link_background = svg 'static/images/button_glitch.svg', { click: proc { glitch } }
      end
    end
  end

  def glitch
    @screen.messages.show_success(@glitch_io.input_folder.present?)
    DatamosherAsync.new(@glitch_io.input_file, @glitch_io.input_folder, @glitch_io.output_filename, @glitch_io.output_folder).async.call
  end
end
