class Shoes::ScreenContentHelp < Shoes::Widget
  def call(screen)
    @screen = screen
    navigation
    stack do
      background Colors.grey
      stack margin_bottom: 20, margin_top: 20 do
        subtitle CONTENT['help']['title'], align: 'center'
        stack margin_left: 20 do
          @help_sections = ['goal', 'input', 'output', 'options', 'sampler']
          @help_sections.each do |section|
            section_title(CONTENT['help'][section.to_s]['title'])
            stack margin_left: 20 do
              para CONTENT['help'][section.to_s]['content']
            end
          end
        end
      end
    end
  end
end
