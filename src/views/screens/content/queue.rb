class Shoes::ScreenContentQueue < Shoes::Widget
  def call(screen)
    @screen = screen
    @queue = update_queue
    navigation('queue')
    queue_content
  end

  private

  def queue_content
    stack do
      background Colors.grey
      stack margin_bottom: 20, margin_top: 20 do
        stack margin_left: 20 do
          section_title(CONTENT['queue']['subtitle'])
          stack margin_left: 20 do
            button('Refresh') { visit '/queue' }
            queue_table
          end
        end
      end
    end
  end

  def queue_table
    return (para CONTENT['queue']['empty_queue']) if @queue.empty?

    stack do
      queue_headers
      @queue.each do |item|
        flow do
          formated_kill_link(item)
          formated_description(item)
        end
      end
    end
  end

  def queue_headers
    flow margin_left: 40 do
      flow width: 140 do
        @algo = para CONTENT['queue']['algorithm']
        @algo.weight = 'bold'
      end
      flow width: 140 do
        @type = para CONTENT['queue']['type']
        @type.weight = 'bold'
      end
    end
  end

  def update_queue
    Thread.list.reject { |t| t == Thread.main }.map { |t| t[:name] }
  end

  def kill_thread_and_update(name)
    kill_thread(name)
    visit '/queue'
  end

  def kill_thread(name)
    thread = Thread.list.select { |t| t[:name] == name }.first
    return if thread.nil?

    thread.kill
  end

  def formated_kill_link(item)
    flow width: 40 do
      para link('kill', click: proc { kill_thread_and_update(item) })
    end
  end

  def formated_description(item)
    description = item.split('-')
    flow width: 140 do
      para description.first.capitalize
    end
    flow width: 140 do
      para description.last.capitalize
    end
  end
end
