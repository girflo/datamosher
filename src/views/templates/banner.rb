require_relative 'colors.rb'

class Shoes::Banner < Shoes::Widget
  def initialize
    flow do
      background Colors.dark_grey
      inner_banner = flow do
        logo = image 'static/images/banner.png'
        logo.height = 80
        logo.width = 460
      end
      inner_banner.displace_left = 10
    end
  end
end
