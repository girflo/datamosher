require_relative 'colors.rb'

class Shoes::SectionTitle < Shoes::Widget
  def initialize(title)
    @section_title = tagline title
    @section_title.stroke = Colors.app_background
  end
end
