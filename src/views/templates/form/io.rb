class TemplateFormIo < Shoes::Widget
  def input_file
    @input_file
  end

  def input_folder
    @input_folder
  end

  def output_folder
    @output_folder
  end

  def output_filename
    @output_filename.text
  end

  def initialize(suffix_filename, filename = 'glitcher')
    stack do
      background Colors.grey
      stack margin_left: 20, margin_top: 20 do
        section_title('Input')
        flow margin_left: 20 do
          button 'Video to datamosh' do
            @input_file = ask_open_file title: 'Choose the video you want to datamosh'
            @input_folder = nil
            update_preview(@input_file)
          end
          para 'OR'
          button 'Directory to datamosh' do
            @input_folder = ask_open_folder title: 'Choose the directory you want to datamosh'
            @input_file = nil
            if !@input_folder.nil?
              update_preview_directory(@input_folder)
            end
          end
          @choosed_input = stack { }
        end
      end
    end
    stack do
      background Colors.grey
      stack margin_left: 20, margin_top: 20 do
        section_title('Output')
        flow margin_left: 20 do
          flow do
            button 'Output directory' do
              @output_folder = ask_open_folder
              @choosed_output.clear { para "Selected directory : #{@output_folder}" }
            end
          end
          @choosed_output = stack {  }
          flow do
            para 'Schema for output filename : '
            @output_filename = edit_line
            @output_filename.text = filename
            para suffix_filename
          end
        end
      end
    end
  end

  private

  def update_preview(path)
    return if path.nil?

    @choosed_input.clear { para "Selected file : #{@input_file}" }
  end

  def update_preview_directory(path)
    avi_files = Dir["#{path}/*.avi"].count
    @choosed_input.clear do
      para "Selected directory : #{path}"
      if avi_files > 1
        para "Containing #{avi_files} avi children"
      else
        para "Containing #{avi_files} avi child"
      end
    end
  end
end
