# coding: utf-8
class TemplateFormMessages < Shoes::Widget
  def hide_all
    @errors_stack.hide
    @success_stack.hide
  end

  def show_success(multiple_images)
    message = success_message(multiple_images)
    @success_message.text = message
    @success_stack.show
    timer(4) do
      @success_stack.hide
    end
  end

  def show_errors(errors)
    @errors.text = errors.each_with_index.map do |m, i|
      if i == errors.size - 1
        "◦ #{m.to_s}"
      else
      "◦ #{m.to_s}
"
      end
    end
    @errors_stack.show
  end

  def initialize(error_title)
    stack margin_left: 40, margin_right: 40 do
      @errors_stack = template_errors(error_title)
      @success_stack = template_success
    end
    @errors_stack.hide
    @success_stack.hide
  end

  private

  def template_errors(title)
    stack margin_top: 12 do
      background Colors.red
      stack margin_left: 10 do
        para title
        @errors = para ''
      end
    end
  end

  def template_success
    stack margin_top: 12 do
      background Colors.light_green
      stack margin_left: 10 do
        @success_message = para ''
      end
    end
  end

  def success_message(multiple_images)
    if multiple_images
      CONTENT['success_multiple']
    else
      CONTENT['success_single']
    end
  end
end
