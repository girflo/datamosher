module Colors
  def self.light_green
    rgb(188, 244, 166)
  end
  
  def self.green
    rgb(171, 208, 206)
  end

  def self.app_background
    rgb(84, 89, 105)
  end

  def self.grey
    rgb(220, 225, 224)
  end

  def self.medium_grey
    rgb(197, 206, 204)
  end
    
  def self.dark_grey
    rgb(49, 41, 53)
  end

  def self.red
    rgb(255, 204, 204)
  end
end
