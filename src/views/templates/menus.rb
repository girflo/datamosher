class Shoes::Menus < Shoes::Widget
  def initialize
    return unless menubar.menus.size == 1
    
    helpmenu = menu "Help"
    @helpitem = menuitem "Help", key: "control_h" do
      visit '/help'
    end
    @aboutitem = menuitem "About" do
      visit '/about'
    end
    helpmenu << @helpitem
    helpmenu << @aboutitem
    datamoshermenu = menu "Datamosher"
    @datamosheritem = menuitem "Datamosher" do
      visit '/'
    end
    @queueitem = menuitem "Queue" do
      visit '/queue'
    end
    datamoshermenu << @datamosheritem
    datamoshermenu << @queueitem
    menubar << datamoshermenu
    menubar << helpmenu
  end
end
