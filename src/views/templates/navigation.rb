require_relative 'colors.rb'

class Shoes::Navigation < Shoes::Widget
  def initialize(current_tab = nil)
    flow margin_top: 12 do
      stack width: 180, height: 40 do
        navigation_button(current_tab, 'datamosher', '/')
      end
      stack width: 140, height: 40, displace_left: 6 do
        navigation_button(current_tab, 'queue', '/queue')
      end
    end
  end

  def navigation_button(current_tab, tab_name, link)
    if current_tab == tab_name
      navigation_button_selected(tab_name)
    else
      navigation_button_unselected(tab_name, link)
    end
  end

  def navigation_button_selected(tab_name)
    background Colors.grey
    @link_background = svg "static/images/nav_#{tab_name}_selected.svg"
  end

  def navigation_button_unselected(tab_name, link)
    background Colors.medium_grey
    @link_background = svg "static/images/nav_#{tab_name}.svg", { click: proc { visit link } }
  end
end
